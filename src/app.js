const http = require('http')
const EventEmitter = require('events')

console.log('Start app.js')

let App = {
  start: function(port) {
    let emitter = new EventEmitter()
    let server = http.createServer((request, response) => {
      response.writeHead(200, {
        'Content-Type': 'text/html; charset=utf-8'
    })
      if (request.url === '/') {
        emitter.emit('root', response)
      }
      response.end()
    }).listen(port)
    return emitter
  }
}

let app = App.start(5555)
app.on('root', function(response){
  response.write(process.env.VERSION)
});