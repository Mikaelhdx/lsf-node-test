FROM node:latest

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

ARG VERSION
ENV VERSION=$VERSION

# Install app dependencies
RUN npm install

# Bundle app source
COPY . /usr/src/app

CMD node src/app.js

